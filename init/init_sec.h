#ifndef INIT_SEC_H
#define INIT_SEC_H

#include <string.h>

#define TOTAL_VARIANTS 3 //Total number of variants

typedef struct {
    std::string model;
    std::string codename;
} variant;

static const variant J530G_model = {
    .model = "SM-J530G",
    .codename = "j5y17lteub"
};

static const variant J530F_model = {
    .model = "SM-J530F",
    .codename = "j5y17ltexx"
};

static const variant J530Y_model = {
    .model = "SM-J530Y",
    .codename = "j5y17ltedx"
};

static const variant *all_variants[TOTAL_VARIANTS] = {
    &J530G_model,
    &J530F_model,
    &J530Y_model,
};

#endif // INIT_SEC_H
